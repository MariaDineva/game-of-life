export enum GenerationButtonTextsEnum {
  StartCustomPlay = 'Start Play With Pre-populated Live Cells',
  StopCustomPlay = 'Stop Play With Pre-populated Live Cells',
  StartRandomPlay = 'Start New Random Play',
  StopRandomPlay = 'Stop Random Play',
  ClearLiveCells = 'Clear Live Cells',
}
