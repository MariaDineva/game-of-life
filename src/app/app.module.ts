import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSliderModule } from '@angular/material/slider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';

import { AppComponent } from './app.component';
import { CellGridComponent } from './cell-grid/cell-grid.component';
import { CellComponent } from './cell/cell.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CellGridOptionsService } from './services/cell-grid-options.service';

@NgModule({
  declarations: [
    AppComponent,
    CellGridComponent,
    CellComponent,
    NavigationComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatToolbarModule,
    MatSidenavModule,
    MatTooltipModule,
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    MatSelectModule,
  ],
  providers: [
    CellGridOptionsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
