import { Component } from '@angular/core';
import { Subject } from 'rxjs';

import { GenerationButtonTextsEnum } from '../enums/generation-button-texts.enum';

@Component({
  selector: 'app-nav',
  templateUrl: 'navigation.component.html',
  styleUrls: ['navigation.component.scss'],
})
export class NavigationComponent {
  public isRandomPlayInProgress = false;
  public isCustomPlayInProgress = false;
  public startRandomPlayButtonText = GenerationButtonTextsEnum.StartRandomPlay;
  public startCustomPlayButtonText = GenerationButtonTextsEnum.StartCustomPlay;
  public clearLiveCellsButtonText = GenerationButtonTextsEnum.ClearLiveCells;
  public ngModel = false;
  public colors = ['black', 'blue', 'purple', 'pink'];
  public shapes = ['square', 'round'];
  public aliveCellColor = 'black';
  public aliveCellShape = 'square';
  public probability;
  public clearGrid: Subject<string> = new Subject();
  public setProbability: Subject<number> = new Subject();

  public selectedColor(event) {
    this.aliveCellColor = event.value.toLowerCase();
  }

  public selectedShape(event) {
    this.aliveCellShape = event.value.toLowerCase();
  }

  public toggleRandomPlay() {
    if (!this.isRandomPlayInProgress) {
      this.isRandomPlayInProgress = true;
      this.changeRandomPlayButtonText();
    } else {
      this.isRandomPlayInProgress = false;
      this.changeRandomPlayButtonText();
    }
  }

  public toggleCustomPlay() {
    if (!this.isCustomPlayInProgress) {
      this.isCustomPlayInProgress = true;
      this.changeCustomPlayButtonText();
    } else {
      this.isCustomPlayInProgress = false;
      this.changeCustomPlayButtonText();
    }
  }

  private changeRandomPlayButtonText() {
    if (!this.isRandomPlayInProgress) {
      this.startRandomPlayButtonText = GenerationButtonTextsEnum.StartRandomPlay;
    } else {
      this.startRandomPlayButtonText = GenerationButtonTextsEnum.StopRandomPlay;
    }
  }

  private changeCustomPlayButtonText() {
    if (!this.isCustomPlayInProgress) {
      this.startCustomPlayButtonText = GenerationButtonTextsEnum.StartCustomPlay;
    } else {
      this.startCustomPlayButtonText = GenerationButtonTextsEnum.StopCustomPlay;
    }
  }

  public clearLiveCells() {
    this.clearGrid.next('clear');
  }

  public getProbabilityValue(event) {
    this.setProbability.next(event.value);
  }
}
