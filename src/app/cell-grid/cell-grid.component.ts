import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';

import { GenerationTypesEnum } from '../enums/generation-types.enum';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-cell-grid',
  templateUrl: 'cell-grid.component.html',
  styleUrls: ['cell-grid.component.scss'],
})
export class CellGridComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public isRandomGenerationInProgress: boolean;
  @Input() public isCustomGenerationInProgress: boolean;
  @Input() public livingCellColor: string;
  @Input() public livingCellShape: string;
  @Input() public removeLiveCells$: Subject<string>;
  @Input() public setLiveCellsProbabilityValue$: Subject<number>;
  public gridState;
  private columns = 40;
  private rows = 40;

  constructor() {
    this.gridState = this.createEmptyGrid(this.columns, this.rows);
  }

  public ngOnInit() {
    this.removeLiveCells$.subscribe(() => {
      this.clearGrid();
    });

    this.setLiveCellsProbabilityValue$.subscribe((value) => {
      this.fillInGridRandomly(value);
    });
  }

  public ngOnChanges() {
    if (this.isRandomGenerationInProgress) {
      this.startRandomGeneration();
    } else if (this.isCustomGenerationInProgress) {
      this.startCustomGeneration();
    }
  }

  public ngOnDestroy() {
    this.removeLiveCells$.unsubscribe();
    this.setLiveCellsProbabilityValue$.unsubscribe();
  }

  private createEmptyGrid(cols: number, rows: number) {
    const emptyGrid = Array
      .from({ length: cols })
      .map(() => Array.from({ length: rows })
        .map(() => 0));

    return emptyGrid;
  }

  private fillInGridRandomly(probabilityValue) {
    let liveCellsProbability: number;
    if (probabilityValue && typeof probabilityValue === 'number') {
      liveCellsProbability = probabilityValue / 10;
    } else {
      liveCellsProbability = Math.random();
    }
    for (let i = 0; i < this.columns; i++) {
      for (let j = 0; j < this.rows; j++) {
        this.gridState[i][j] = Math.floor(Math.random() < liveCellsProbability as any);
      }
    }
  }

  public fillInNextGeneration() {
    const nextGeneration = this.createEmptyGrid(this.columns, this.rows);
    for (let i = 0; i < this.columns; i++) {
      for (let j = 0; j < this.rows; j++) {
        const currentCellState = this.gridState[i][j];

        const neighborsCount = this.countNeighbors(this.gridState, i, j);
        // If a dead cell has 3 live neighbors, it becomes alive
        if (currentCellState === 0 && neighborsCount === 3) {
          nextGeneration[i][j] = 1;
        } else if (currentCellState === 1 && (neighborsCount < 2 || neighborsCount > 3)) {
          // If an alive cell has more that 3 or less then 2 live neighbors, it becomes dead
          nextGeneration[i][j] = 0;
        } else {
          // In all other cases the cell stays as it was
          nextGeneration[i][j] = currentCellState;
        }
      }
    }

    this.gridState = nextGeneration;
  }

  private startGeneration(generationType) {
    if (generationType === GenerationTypesEnum.Random) {
      const intervalId = setInterval(() => {
        this.fillInNextGeneration();
        if (!this.isRandomGenerationInProgress) {
          clearInterval(intervalId);
        }
      }, 100);
    } else {
      const intervalId = setInterval(() => {
        this.fillInNextGeneration();
        if (!this.isCustomGenerationInProgress) {
          clearInterval(intervalId);
        }
      }, 100);
    }
  }

  private startRandomGeneration() {
    this.fillInGridRandomly(null);
    this.startGeneration(GenerationTypesEnum.Random);
  }

  private startCustomGeneration() {
    this.startGeneration(GenerationTypesEnum.Custom);
  }



  private countNeighbors(matrix, x, y) {
    let sum = 0;

    // This is how we need to calculate the neighbours, but we are doing it in the loop below:
    // sum += this.gridState[i - 1][j - 1];
    // sum += this.gridState[i - 1][j];
    // sum += this.gridState[i - 1][j + 1];
    // sum += this.gridState[i][j - 1];
    // sum += this.gridState[i][j + 1];
    // sum += this.gridState[i + 1][j - 1];
    // sum += this.gridState[i + 1][j];
    // sum += this.gridState[i + 1][j + 1];
    for (let i = -1; i < 2; i++) {
      for (let j = -1; j < 2; j++) {
        //
        /* Use modulus operator to get the remainder of division
        /* and presume the the neighbor of the edge cell
        /* is the cell at the other end of the grid
        */
        let col = (x + i + this.columns) % this.columns;
        let row = (y + j + this.rows) % this.rows;
        sum += matrix[col][row];
      }
    }

    // Subtract the element itself as it should not be counted
    sum -= matrix[x][y];

    return sum;
  }

  public toggleCellState(x, y) {
    this.gridState[x][y] = this.gridState[x][y] === 1 ? 0 : 1;
  }

  private clearGrid() {
    for (let i = 0; i < this.columns; i++) {
      for (let j = 0; j < this.rows; j++) {
        this.gridState[i][j] = 0;
      }
    }
  }
}
