import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: 'cell.component.html',
  styleUrls: ['cell.component.scss'],
})
export class CellComponent {
  @Input() state: number;
  @Input() public aliveCellColor = 'black';
  @Input() public aliveCellShape = 'square';

  public get classes() {
    return `thumbnail ${this.state ? this.aliveCellColor : ''} ${this.aliveCellShape}`;
  }
}
