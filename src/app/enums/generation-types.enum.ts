export enum GenerationTypesEnum {
  Custom = 'custom',
  Random = 'random'
}
